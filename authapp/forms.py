from django.forms import CharField, EmailField
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm
from django.contrib.auth.models import User


class SignUpForm(UserCreationForm):
    email = EmailField(max_length=255)
    first_name = CharField(max_length=20)
    last_name = CharField(max_length=20)

    def __init__(self, *args, **kwargs) -> None:
        super(SignUpForm, self).__init__(*args, **kwargs)
        del self.fields["password2"]

    class Meta:
        model = User
        fields = [
            "username",
            "email",
            "first_name",
            "last_name",
            "password1",
        ]


class LoginForm(AuthenticationForm):
    def __init__(self, request=None, *args, **kwargs) -> None:
        super().__init__(request=request, *args, **kwargs)
        self.error_messages["invalid_login"] = "Username or password is invalid"
