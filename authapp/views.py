from django.shortcuts import redirect, render
from django.contrib.auth.forms import AuthenticationForm
from django.contrib.auth import login as auth_login, logout as auth_logout
from django.contrib.auth.decorators import login_required

from authapp.forms import SignUpForm, LoginForm


def signup(request):
    form = SignUpForm(request.POST or None)
    if request.method == "POST":
        if form.is_valid():
            form.save()
            return redirect("authapp:login")

    return render(request, "authapp/signup.j2", context={"form": form})


def login(request):
    form = LoginForm(data=(request.POST or None))
    if request.method == "POST":
        if form.is_valid():
            auth_login(request, form.get_user())
            return redirect("blog:home")

    return render(request, "authapp/login.j2", context={"form": form})


@login_required(login_url="authapp:login")
def logout(request):
    auth_logout(request)
    return redirect("authapp:login")
