FROM alpine:3.13

# set work directory
WORKDIR /opt/blog

# set environment variables
ENV PYTHONUNBUFFERED 1

# install python
RUN apk update && apk add py3-pip python3-dev postgresql-dev musl-dev gcc

# install dependencies
COPY ./requirements.txt ./
RUN pip install -r requirements.txt

# copy project
COPY ./ ./

# open 8000 port
EXPOSE 8000

# make entry command and default command
RUN ["chmod", "+x", "docker-entrypoint.sh"]
ENTRYPOINT ["./docker-entrypoint.sh"]
CMD ["runserver", "0.0.0.0:8000"]
