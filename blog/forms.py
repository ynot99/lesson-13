from django.forms import ModelForm

from .models import BlogItem, Category


class AddBlogItemForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(AddBlogItemForm, self).__init__(*args, **kwargs)
        for visible in self.visible_fields():
            visible.field.widget.attrs["class"] = "blog-form__field"

    class Meta:
        model = BlogItem
        fields = ["header", "category", "content"]


class AddCategoryForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(AddCategoryForm, self).__init__(*args, **kwargs)
        for visible in self.visible_fields():
            visible.field.widget.attrs["class"] = "blog-form__field"

    class Meta:
        model = Category
        fields = ["name"]
