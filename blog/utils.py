from .models import Category


def get_default_render_data(exclude: list = []) -> dict:
    """Prepares default data for render blog pages

    Args:
        exclude (list, optional): list of keys to exclude. Defaults to [].

    Returns:
        dict: default render data
    """
    data = {}

    if "categories" not in exclude:
        data["categories"] = Category.objects.all()

    return data
