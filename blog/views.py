from django.shortcuts import redirect, render
from django.contrib.auth.models import User
from django.contrib.auth.decorators import login_required
from django.contrib.admin.views.decorators import staff_member_required
from django.http import Http404

from .utils import get_default_render_data
from .forms import AddBlogItemForm, AddCategoryForm
from .models import BlogItem, Category


def home(request):
    blog_items = BlogItem.objects.all().order_by("-created_at")

    return render(
        request,
        "blog/home.j2",
        context={"blog_items": blog_items, **get_default_render_data()},
    )


def item(request, id):
    blog_item = None
    try:
        blog_item = BlogItem.objects.get(pk=id)
    except BlogItem.DoesNotExist:
        raise Http404

    return render(
        request,
        "blog/item.j2",
        context={"blog_item": blog_item, **get_default_render_data()},
    )


def category(request, id):
    category = None
    try:
        category = Category.objects.get(pk=id)
    except Category.DoesNotExist:
        raise Http404
    blog_items = BlogItem.objects.filter(category=category).order_by(
        "-created_at"
    )

    return render(
        request,
        "blog/category.j2",
        context={
            "blog_items": blog_items,
            "category": category,
            **get_default_render_data(),
        },
    )


def user(request, id):
    user = None
    try:
        user = User.objects.get(pk=id)
    except User.DoesNotExist:
        raise Http404

    blog_items = BlogItem.objects.filter(author=user).order_by("-created_at")

    return render(
        request,
        "blog/user.j2",
        context={
            "blog_items": blog_items,
            "username": user.username,
            **get_default_render_data(),
        },
    )


@login_required
def add_blog_item(request):
    form = AddBlogItemForm(request.POST or None)
    if request.method == "POST":
        if form.is_valid():
            obj = form.save(commit=False)
            obj.author = request.user
            obj.save()
            return redirect("blog:home")

    return render(
        request,
        "blog/add_blog_item.j2",
        context={"form": form, **get_default_render_data()},
    )


@staff_member_required
def add_category(request):
    form = AddCategoryForm(request.POST or None)
    if request.method == "POST":
        if form.is_valid():
            form.save()
            return redirect("blog:home")

    return render(
        request,
        "blog/add_category.j2",
        context={"form": form, **get_default_render_data()},
    )


def generate_test_data(request):
    # PREPARE DATA
    author = {"username": "test", "password": "test"}

    categories = [
        {"name": "Test science"},
        {"name": "Test life"},
        {"name": "Test health"},
    ]

    blog_items = [
        {
            "header": "Babies have around 100 more bones than adults",
            "category": 0,
            "content": "Babies have about 300 bones at birth, with cartilage between many of them. This extra flexibility helps them pass through the birth canal and also allows for rapid growth. With age, many of the bones fuse, leaving 206 bones that make up an average adult skeleton.",
        },
        {
            "header": "20% of Earth’s oxygen is produced by the Amazon rainforest",
            "category": 0,
            "content": "Our atmosphere is made up of roughly 78 per cent nitrogen and 21 per cent oxygen, with various other gases present in small amounts. The vast majority of living organisms on Earth need oxygen to survive, converting it into carbon dioxide as they breathe. Thankfully, plants continually replenish our planet’s oxygen levels through photosynthesis. During this process, carbon dioxide and water are converted into energy, releasing oxygen as a by-product. Covering 5.5 million square kilometres (2.1 million square miles), the Amazon rainforest cycles a significant proportion of the Earth’s oxygen, absorbing large quantities of carbon dioxide at the same time.",
        },
        {
            "header": "You're tallest first thing in the morning",
            "category": 1,
            "content": 'If you\'re feeling a bit on the short side, measure yourself when you first wake up. According to Jamaica Hospital Medical Center, This phenomenon may be due to gravity compressing cartilage in our spine and in other parts of our bodies, such as our knees when we stand up or sit down throughout the day. "While we are lying down in a resting position, the spine is said to "spread out" or decompress, so when we wake in the morning we are taller after lying in bed all night."',
        },
        {
            "header": "You should sleep with your door closed",
            "category": 1,
            "content": "Sleeping with your doors closed will help protect you from smoke and toxic fumes in the event of a fire. And for more information on how to rest better at night, check out these 50 Tips for Your Best Sleep Ever.",
        },
        {
            "header": "You typically only breathe through one nostril at a time",
            "category": 2,
            "content": "You might think that your nostrils share the workload when it comes to inhaling and exhaling. And while they do, it's not quite in the way that you might expect. You actually inhale and exhale through one nostril at a time, according to definitive research published in the journal Mayo Clinic Proceedings in 1977. Every few hours, the active nostril will take a break and the other one will take over until they ultimately switch back again. Put your finger under your nose and try it. Prepare to be amazed!",
        },
        {
            "header": "Eating eggs improves your reflexes",
            "category": 2,
            "content": "If you want to be able to respond more quickly, you can start by having an omelette for breakfast. Eggs contain an amino acid called tyrosine, which the body synthesizes into norepinephrine and dopamine, compounds that increase energy and alertness and improve mood. In a 2014 study published in the journal Neuropsychologia, researchers even found that tyrosine enhances our response time and improves our intellectual performance, not unlike a medical stimulant like Ritalin or Modafinil.",
        },
    ]

    # DELETE DATA
    User.objects.filter(username="test").delete()

    Category.objects.filter(name__in=[c["name"] for c in categories]).delete()

    BlogItem.objects.filter(
        header__in=[bi["header"] for bi in blog_items]
    ).delete()

    # INSERT DATA
    author = User.objects.create_user(
        username=author["username"], password=author["password"]
    )

    new_categories = []
    for c in categories:
        new_categories.append(Category.objects.create(name=c["name"]))

    BlogItem.objects.bulk_create(
        BlogItem(
            header=bi["header"],
            author=author,
            category=new_categories[bi["category"]],
            content=bi["content"],
        )
        for bi in blog_items
    )

    return render(request, "blog/test_data.j2")
