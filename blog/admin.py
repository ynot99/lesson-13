from django.contrib import admin

from .models import Category, BlogItem


class BlogItemAdmin(admin.ModelAdmin):
    list_display = [
        "header",
        "category",
        "author",
        "updated_at",
        "created_at",
    ]


admin.site.register(Category)
admin.site.register(BlogItem, BlogItemAdmin)
