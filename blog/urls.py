from django.urls import path

from . import views


app_name = "blog"

urlpatterns = [
    path("", views.home, name="home"),
    path("<int:id>", views.item, name="item"),
    path("category/<int:id>", views.category, name="category"),
    path("user/<int:id>", views.user, name="user"),
    path("add_blog_item/", views.add_blog_item, name="add_blog_item"),
    path("add_category/", views.add_category, name="add_category"),
    path("test_data/", views.generate_test_data, name="test_data"),
]
