from django.db import models
from django.contrib.auth.models import User


class Category(models.Model):
    name = models.CharField(
        max_length=55, unique=True, help_text="This is a unique field"
    )

    def __str__(self) -> str:
        return self.name

    class Meta:
        verbose_name_plural = "Categories"


class BlogItem(models.Model):
    header = models.CharField(
        max_length=100, unique=True, help_text="This is a unique field"
    )
    category = models.ForeignKey(
        to=Category, on_delete=models.SET_NULL, null=True, blank=True
    )
    content = models.TextField()
    author = models.ForeignKey(
        to=User, on_delete=models.SET_NULL, null=True, blank=True
    )
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self) -> str:
        return self.header
