// FORM FIELDS EVENTS
const form_fields = document.getElementsByClassName("form__field");

const handleFocus = (e) => {
   e.srcElement.parentNode
      .getElementsByClassName("form__label")[0]
      .classList.add("focused");
};

const handleBlur = (e) => {
   e.srcElement.parentNode
      .getElementsByClassName("form__label")[0]
      .classList.remove("focused");
};

const handleChange = (e) => {
   const currentValue = e.target.value;
   const class_list =
      e.srcElement.parentNode.getElementsByClassName("form__label")[0]
         .classList;
   if (currentValue) {
      class_list.add("has_value");
   } else if (!currentValue) {
      class_list.remove("has_value");
   }
};

for (let form_field of form_fields) {
   const input = form_field.getElementsByTagName("input")[0];
   input.onfocus = handleFocus;
   input.onblur = handleBlur;
   input.onchange = handleChange;
   input.dispatchEvent(new Event("change"));
}

document.addEventListener("DOMContentLoaded", () => {
   const input = form_fields[0].getElementsByTagName("input")[0];
   input.blur(); // TODO remove crutch, focus event not triggering if don't call blur function first
   input.focus();
});

// FORM EVENTS
const form = document.getElementById("form");
const form_container = document.getElementById("form-container");

const handleMouseEnter = () => {
   form.style.transition = "none";
};
const handleMouseMove = (e) => {
   const halfFormWidth = form.clientWidth / 2;
   const halFormHeight = form.clientHeight / 2;
   const offsetX = e.pageX - form.offsetLeft;
   const offsetY = e.pageY - form.offsetTop;
   const transformX = -((offsetY - halFormHeight) / halFormHeight);
   const transformY = (offsetX - halfFormWidth) / halfFormWidth;
   form.style.transform = `perspective(10cm) rotateX(${transformX}deg) rotateY(${transformY}deg)`;
};
const handleMouseLeave = () => {
   form.style.transition = "all 0.3s ease";
   form.style.transform = `perspective(10cm) rotateX(0deg) rotateY(0deg)`;
};

form_container.onmouseenter = handleMouseEnter;
form_container.onmousemove = handleMouseMove;
form_container.onmouseleave = handleMouseLeave;
